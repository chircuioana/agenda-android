package com.hb.agenda.di.modules

import androidx.lifecycle.ViewModel
import com.hb.agenda.di.scopes.ChildFragmentScoped
import com.hb.agenda.di.scopes.ViewModelKey
import com.hb.agenda.ui.addedit.AddOrEditContactFragment
import com.hb.agenda.ui.addedit.AddOrEditContactViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
@Suppress("UNUSED")
internal abstract class AddContactModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun addContactFragment(): AddOrEditContactFragment

    @Binds
    @IntoMap
    @ViewModelKey(AddOrEditContactViewModel::class)
    abstract fun bindContactsListViewModel(viewModel: AddOrEditContactViewModel): ViewModel
}
