package com.hb.agenda.di.modules

import com.hb.agenda.MainActivity
import com.hb.agenda.di.scopes.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            // activity
            MainActivityModule::class,
            // fragments
            ContactsListModule::class,
            AddContactModule::class
        ]
    )
    internal abstract fun mainActivity(): MainActivity
}