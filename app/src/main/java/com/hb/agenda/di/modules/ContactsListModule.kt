package com.hb.agenda.di.modules

import androidx.lifecycle.ViewModel
import com.hb.agenda.di.scopes.ChildFragmentScoped
import com.hb.agenda.di.scopes.ViewModelKey
import com.hb.agenda.ui.list.ContactsListFragment
import com.hb.agenda.ui.list.ContactsListViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
@Suppress("UNUSED")
internal abstract class ContactsListModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contactsListFragment(): ContactsListFragment

    @Binds
    @IntoMap
    @ViewModelKey(ContactsListViewModel::class)
    abstract fun bindContactsListViewModel(viewModel: ContactsListViewModel): ViewModel
}
