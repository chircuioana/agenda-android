package com.hb.agenda.di.modules

import androidx.lifecycle.ViewModelProvider
import com.hb.agenda.di.AgendaViewModelFactory
import dagger.Binds
import dagger.Module

@Module
@Suppress("UNUSED")
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: AgendaViewModelFactory):
            ViewModelProvider.Factory
}