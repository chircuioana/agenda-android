package com.hb.agenda.ui.addedit

import androidx.lifecycle.*
import com.hb.shared.agenda.entities.*
import com.hb.shared.agenda.interactors.AddContactUseCase
import com.hb.shared.agenda.interactors.EditContactUseCase
import com.hb.shared.agenda.interactors.GetContactForPhoneNumberUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

enum class Mode {
    ADD,
    EDIT
}

class AddOrEditContactViewModel @Inject constructor(
    val addContactUseCase: AddContactUseCase,
    val editContactUseCase: EditContactUseCase,
    val getContactUseCase: GetContactForPhoneNumberUseCase
) : ViewModel() {

    val firstName: LiveData<String> get() = _firstName
    private val _firstName = MutableLiveData<String>()

    val lastName: LiveData<String> get() = _lastName
    private val _lastName = MutableLiveData<String>()

    val phoneNumber: MutableLiveData<String> get() = _phoneNumber
    private val _phoneNumber = MutableLiveData<String>()

    val email: LiveData<String> get() = _email
    private val _email = MutableLiveData<String>()

    val errors: LiveData<List<Int>> get() = _errors
    private val _errors = MutableLiveData<List<Int>>()

    val isLoading: MutableLiveData<Boolean> get() = _isLoading
    private val _isLoading = MutableLiveData<Boolean>()

    val mode: LiveData<Mode> get() = _mode
    private val _mode = MutableLiveData<Mode>()

    var selectedContactPhoneNumber: String? = null
        set(value) {
            viewModelScope.launch {
                field = value
                value ?: return@launch
                _mode.postValue(Mode.EDIT)
                getContactUseCase(
                    GetContactForPhoneNumberUseCase.Params(value)
                ).collect {
                    when (it) {
                        is Resource.Success -> {
                            it.data.run {
                                _firstName.postValue(this.firstName)
                                _lastName.postValue(this.lastName)
                                _phoneNumber.postValue(this.phoneNumber)
                                _email.postValue(this.email)
                            }
                            _isLoading.postValue(false)
                        }
                        is Resource.Error -> {
                            _errors.postValue(it.codes)
                            _isLoading.postValue(false)
                        }
                        is Resource.Loading -> {
                            _isLoading.postValue(true)
                        }
                    }
                }
            }
        }

    init {
        _mode.postValue(Mode.ADD)
    }

    fun setFirstName(name: String) = viewModelScope.launch {
        _firstName.postValue(name)
    }

    fun setLastName(name: String) = viewModelScope.launch {
        _lastName.postValue(name)
    }

    fun setPhoneNumber(phoneNumber: String) = viewModelScope.launch {
        _phoneNumber.postValue(phoneNumber)
    }

    fun setEmail(phoneNumber: String) = viewModelScope.launch {
        _email.postValue(phoneNumber)
    }

    fun finish(): LiveData<Unit> {
        return when (_mode.value ?: Mode.ADD) {
            Mode.ADD -> {
                addMember()
            }
            Mode.EDIT -> {
                editMember()
            }
        }
    }

    private fun addMember(): LiveData<Unit> {
        val result = MutableLiveData<Unit>()
        viewModelScope.launch {
            val firstName = _firstName.value
            val lastName = _lastName.value
            val phoneNumber = _phoneNumber.value
            val email = _email.value

            addContactUseCase(
                AddContactUseCase.Params(
                    firstName = firstName,
                    lastName = lastName,
                    phoneNumber = phoneNumber,
                    email = email
                )
            )
                .flowOn(Dispatchers.Main)
                .collect {
                    when (it) {
                        is Resource.Success -> {
                            result.postValue(Unit)
                            _isLoading.postValue(false)
                        }
                        is Resource.Error -> {
                            _errors.postValue(ArrayList(it.codes ?: arrayListOf()))
                            _isLoading.postValue(false)
                        }
                        is Resource.Loading -> {
                            _isLoading.postValue(true)
                        }
                    }
                }
        }
        return result
    }

    private fun editMember(): LiveData<Unit> {
        val result = MutableLiveData<Unit>()
        viewModelScope.launch {
            val firstName = _firstName.value
            val lastName = _lastName.value
            val phoneNumber = _phoneNumber.value
            val email = _email.value

            editContactUseCase(
                EditContactUseCase.Params(
                    firstName = firstName,
                    lastName = lastName,
                    phoneNumber = phoneNumber,
                    email = email
                )
            )
                .flowOn(Dispatchers.Main)
                .collect {
                    when (it) {
                        is Resource.Success -> {
                            result.postValue(Unit)
                            _isLoading.postValue(false)
                        }
                        is Resource.Error -> {
                            _errors.postValue(ArrayList(it.codes ?: arrayListOf()))
                            _isLoading.postValue(false)
                        }
                        is Resource.Loading -> {
                            _isLoading.postValue(true)
                        }
                    }
                }
        }
        return result
    }
}