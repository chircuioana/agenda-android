package com.hb.agenda.ui.list

import android.content.Context
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hb.agenda.NavigationCallback
import com.hb.agenda.R
import com.hb.agenda.utils.*
import com.hb.shared.agenda.entities.Contact
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_contacts_list.*
import kotlinx.android.synthetic.main.layout_search_backdrop.*
import javax.inject.Inject

class ContactsListFragment : DaggerFragment() {

    companion object {
        fun newInstance(): ContactsListFragment {
            return ContactsListFragment()
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ContactsListViewModel

    private lateinit var headerMotionChoreographer: HeaderMotionChoreographer
    private lateinit var contactAdapter: ContactsAdapter
    private var activityCallback: NavigationCallback? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        activityCallback = requireActivity() as? NavigationCallback
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        return inflater.inflate(R.layout.fragment_contacts_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupHeader()
        setupRecyclerView()
        observe()
    }

    override fun onDetach() {
        super.onDetach()
        activityCallback = null
    }

    private fun setupHeader() {
        headerMotionChoreographer = HeaderMotionChoreographer(
            viewLifecycleOwner,
            lHeader as MultiListenerMotionLayout
        )
    }

    private fun setupRecyclerView() {
        contactAdapter = ContactsAdapter(
            onContactClicked = { contact, isExpanded ->
                viewModel.onExpand(contact, isExpanded)
            },
            onDeleteClicked = { contact ->
                attemptDeleteContact(contact)
            },
            onEditClicked = { contact ->
                activityCallback?.onEditContact(contact)
            })
        rvContacts.apply {
            adapter = contactAdapter
            layoutManager = LinearLayoutManager(requireContext())
//            itemAnimator = SpringAddItemAnimator()
            addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL).apply {
                ContextCompat.getDrawable(requireContext(), R.drawable.divider_gray)?.let {
                    setDrawable(InsetDrawable(it, requireContext().convertDpToPx(75), 0, 0, 0))
                }
            })
        }
    }

    private fun observe() = viewModel.run {
        error.observe(viewLifecycleOwner, Observer {
            //TODO
        })
        loading.observe(viewLifecycleOwner, Observer {
            //TODO
        })
        contacts.observe(viewLifecycleOwner, Observer {
            contactAdapter.contacts = it
        })
        expandedPhones.observe(viewLifecycleOwner, Observer {
            contactAdapter.expandedPhoneList = it
        })
        loadContacts()
    }

    private fun attemptDeleteContact(contact: Contact) {
        AlertDialog.Builder(ContextThemeWrapper(requireContext(), R.style.AlertDialogTheme))
            .setTitle(
                SpannableString(
                    getString(
                        R.string.warning_delete_contact,
                        contact.displayName
                    )
                )
            )
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.deleteContact(contact).observe(viewLifecycleOwner, Observer {
                    toast("Success")
                })
            }
            .setNegativeButton(android.R.string.no) { _, _ -> }
            .show()
    }
}