package com.hb.agenda.ui.addedit

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hb.agenda.NavigationCallback
import com.hb.agenda.R
import com.hb.agenda.utils.updateTextIfDistinct
import com.hb.agenda.utils.viewModelProvider
import com.hb.shared.agenda.entities.*
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_add_contact.*
import javax.inject.Inject

const val CONTACT_PHONE_NUMBER_KEY = "CONTACT_PHONE_NUMBER_KEY"

class AddOrEditContactFragment : BottomSheetDialogFragment() {

    companion object {
        fun newInstance(contactPhoneNumber: String? = null): AddOrEditContactFragment {
            return AddOrEditContactFragment().apply {
                arguments = Bundle().apply {
                    putString(CONTACT_PHONE_NUMBER_KEY, contactPhoneNumber)
                }
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AddOrEditContactViewModel

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_add_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = viewModelProvider(viewModelFactory)
        arguments?.getString(CONTACT_PHONE_NUMBER_KEY)?.let {
            viewModel.selectedContactPhoneNumber = it
        }

        setupControls()
        observe()
    }

    private fun setupControls() {
        etFirstName.doOnTextChanged { text, _, _, _ ->
            viewModel.setFirstName(text?.toString() ?: "")
        }
        etLastName.doOnTextChanged { text, _, _, _ ->
            viewModel.setLastName(text?.toString() ?: "")
        }
        etPhoneNumber.doOnTextChanged { text, _, _, _ ->
            viewModel.setPhoneNumber(text?.toString() ?: "")
        }
        etEmail.doOnTextChanged { text, _, _, _ ->
            viewModel.setEmail(text?.toString() ?: "")
        }
        btDone.setOnClickListener {
            viewModel.finish().observe(viewLifecycleOwner, Observer {
                //TODO show some text
                dismiss()
            })
        }
    }

    private fun observe() = viewModel.run {
        isLoading.observe(viewLifecycleOwner, Observer {
            //TODO
        })
        errors.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            updateFirstNameErrorState(it)
            updateLastNameErrorState(it)
            updatePhoneErrorState(it)
            updateEmailErrorState(it)
        })
        mode.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            when (it) {
                Mode.ADD -> {
                    tvTitle.text = getString(R.string.title_add_contact)
                    tilContactPhoneNumber.isEnabled = true
                }
                Mode.EDIT -> {
                    tvTitle.text = getString(R.string.title_edit_contact)
                    tilContactPhoneNumber.isEnabled = false
                }
            }
        })
        firstName.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            etFirstName.updateTextIfDistinct(it)
        })
        lastName.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            etLastName.updateTextIfDistinct(it)
        })
        phoneNumber.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            etPhoneNumber.updateTextIfDistinct(it)
        })
        email.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            etEmail.updateTextIfDistinct(it)
        })
    }

    private fun updateFirstNameErrorState(errors: List<Int>) = tilContactFirstName.run {
        if (errors.contains(ERROR_FIRST_NAME_IS_EMPTY)) {
            error = getString(R.string.error_field_is_empty)
            return
        }

        error = null
    }

    private fun updateLastNameErrorState(errors: List<Int>) = tilContactLastName.run {
        if (errors.contains(ERROR_LAST_NAME_IS_EMPTY)) {
            error = getString(R.string.error_field_is_empty)
            return
        }

        error = null
    }

    private fun updatePhoneErrorState(errors: List<Int>) = tilContactPhoneNumber.run {
        if (errors.contains(ERROR_PHONE_NOT_FOUND)) {
            //TODO something is wrong
            dismiss()
        }

        if (errors.contains(ERROR_PHONE_IS_EMPTY)) {
            error = getString(R.string.error_field_is_empty)
            return
        }

        if (errors.contains(ERROR_PHONE_FORMAT_INVALID)) {
            error = getString(R.string.error_phone_invalid)
            return
        }

        if (errors.contains(ERROR_PHONE_EXISTS_ALREADY)) {
            error = getString(R.string.error_phone_already_exists)
            return
        }

        error = null
    }

    private fun updateEmailErrorState(errors: List<Int>) = tilContactEmail.run {
        if (errors.contains(ERROR_EMAIL_INVALID)) {
            error = getString(R.string.error_email_invalid)
            return
        }

        error = null
    }
}