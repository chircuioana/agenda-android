package com.hb.agenda.ui.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.hb.agenda.R
import com.hb.shared.agenda.entities.Contact
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactsAdapter(
    val onContactClicked: (Contact, Boolean) -> Unit,
    val onEditClicked: (Contact) -> Unit,
    val onDeleteClicked: (Contact) -> Unit
) :
    RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    var contacts: List<Contact> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var expandedPhoneList = setOf<String>()
        set(value) {
            field = value
//            notifyDataSetChanged()
        }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_contact,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(contacts[position])
    }

    override fun getItemCount(): Int = contacts.size

    override fun getItemId(position: Int): Long = contacts[position].phoneNumber.hashCode().toLong()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView.rootView) {
        private var isExpanded = false
            set(value) {
                if (field == value) return
                field = value
                refreshExpandedState()
            }

        @SuppressLint("SetTextI18n")
        fun bind(contact: Contact) = itemView.run {
            tvName.text = "${contact.firstName} ${contact.lastName}"
            tvPhoneNumber.text = contact.phoneNumber
            tvEmail.text = contact.email
            tvEmail.visibility = if (contact.email.isNullOrEmpty()) {
                GONE
            } else {
                VISIBLE
            }
            isExpanded = expandedPhoneList.contains(contact.phoneNumber)

            setOnClickListener {
                isExpanded = !isExpanded
                onContactClicked(contact, isExpanded)
            }

            btEdit.setOnClickListener {
                onEditClicked(contact)
            }

            btDelete.setOnClickListener {
                onDeleteClicked(contact)
            }
        }

        private fun refreshExpandedState() = itemView.run {
            if (isExpanded) {
                clDetails.visibility = VISIBLE
            } else {
                clDetails.visibility = GONE
            }
            TransitionManager.beginDelayedTransition(itemView.rootView as ViewGroup)
        }
    }
}