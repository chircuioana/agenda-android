package com.hb.agenda.ui.list

import androidx.lifecycle.*
import com.hb.shared.agenda.entities.Contact
import com.hb.shared.agenda.entities.Resource
import com.hb.shared.agenda.interactors.GetAllContactsUseCase
import com.hb.shared.agenda.interactors.RemoveContactUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

class ContactsListViewModel @Inject constructor(
    val getAllContactsUseCase: GetAllContactsUseCase,
    val removeContactUseCase: RemoveContactUseCase
) : ViewModel() {

    val contacts: LiveData<List<Contact>> get() = _contacts
    private val _contacts = MediatorLiveData<List<Contact>>()

    val expandedPhones: LiveData<Set<String>> get() = _expandedPhones
    private val _expandedPhones = MediatorLiveData<Set<String>>()

    val loading: LiveData<Boolean> get() = _loading
    private val _loading = MutableLiveData<Boolean>()

    val error: LiveData<String> get() = _error
    private val _error = MutableLiveData<String>()

    private val _deleteContactResult = MutableLiveData<Unit>()

    init {
        _expandedPhones.postValue(setOf())
    }

    @ExperimentalCoroutinesApi
    fun loadContacts() = viewModelScope.launch {
        val params = GetAllContactsUseCase.Params(query = "")
        getAllContactsUseCase(params)
            .flowOn(Dispatchers.Main)
            .collect {
                when (it) {
                    is Resource.Success -> {
                        _contacts.postValue(it.data)
                        _loading.postValue(false)
                    }
                    is Resource.Error -> {
                        _error.postValue(it.throwable?.localizedMessage ?: "")
                        _loading.postValue(false)
                    }
                    is Resource.Loading -> {
                        _loading.postValue(true)
                    }
                }
            }
    }

    @ExperimentalCoroutinesApi
    fun deleteContact(contact: Contact): LiveData<Unit> {
        viewModelScope.launch {
            removeContactUseCase(RemoveContactUseCase.Params(contact.phoneNumber))
                .flowOn(Dispatchers.Main)
                .collect {
                    when (it) {
                        is Resource.Success -> {
                            _deleteContactResult.postValue(Unit)
                            _loading.postValue(false)
                        }
                        is Resource.Error -> {
                            _error.postValue(it.throwable?.localizedMessage ?: "")
                            _loading.postValue(false)
                        }
                        is Resource.Loading -> {
                            _loading.postValue(true)
                        }
                    }
                }
        }
        return _deleteContactResult
    }

    fun onExpand(contact: Contact, isExpanded: Boolean) {
        val expandedPhones = HashSet(_expandedPhones.value ?: setOf())
        if (isExpanded && !expandedPhones.contains(contact.phoneNumber)) {
            expandedPhones.add(contact.phoneNumber)
        } else if (!isExpanded) {
            expandedPhones.remove(contact.phoneNumber)
        }
        _expandedPhones.postValue(expandedPhones)
    }
}