package com.hb.agenda.ui.list

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.hb.agenda.R
import com.hb.agenda.utils.MultiListenerMotionLayout
import kotlinx.android.synthetic.main.layout_search_backdrop.view.*
import kotlinx.coroutines.launch

enum class HeaderState {
    SEARCH_COLLAPSED,
    HEADER_EXPANDED
}

class HeaderMotionChoreographer(
    private val parent: LifecycleOwner,
    private val header: MultiListenerMotionLayout
) {

    var headerState: HeaderState = HeaderState.SEARCH_COLLAPSED

    init {
        enableClicks()
    }

    private fun expandSearch() = performAnimation {
        header.run {
            setTransition(R.id.start, R.id.mid)
            transitionToState(R.id.mid)
            awaitTransitionComplete(R.id.mid)

            setTransition(R.id.mid, R.id.end)
            transitionToState(R.id.end)
            awaitTransitionComplete(R.id.end)
            headerState = HeaderState.HEADER_EXPANDED
        }
    }

    private fun collapseSearch() = performAnimation {
        header.run {
            setTransition(R.id.end, R.id.mid)
            transitionToState(R.id.mid)
            awaitTransitionComplete(R.id.mid)

            setTransition(R.id.mid, R.id.start)
            transitionToState(R.id.start)
            awaitTransitionComplete(R.id.start)
            headerState = HeaderState.SEARCH_COLLAPSED
        }
    }

    private fun enableClicks() = header.run {
        when (headerState) {
            HeaderState.SEARCH_COLLAPSED -> {
                btSearchExpand.setOnClickListener {
                    expandSearch()
                }
                btSearchCollapse.setOnClickListener(null)
                tilSearch.isEnabled = false
            }
            HeaderState.HEADER_EXPANDED -> {
                btSearchExpand.setOnClickListener(null)
                btSearchCollapse.setOnClickListener {
                    collapseSearch()
                }
                tilSearch.isEnabled = true
            }
        }
    }

    private fun disableClicks() = header.run {
        btSearchExpand.setOnClickListener(null)
        btSearchCollapse.setOnClickListener(null)
        tilSearch.isEnabled = false
    }

    /**
     * Convenience method to launch a coroutine in MainActivity's lifecycleScope
     * (to start animating transitions in MotionLayout) and to handle clicks appropriately.
     *
     * Note: [block] must contain only animation related code. Clicks are
     * disabled at start and enabled at the end.
     *
     * Warning: [awaitTransitionComplete] must be called for the final state at the end of
     * [block], otherwise [enableClicks] will be called at the wrong time for the wrong state.
     */
    private inline fun performAnimation(crossinline block: suspend () -> Unit) {
        parent.lifecycleScope.launch {
            disableClicks()
            block()
            enableClicks()
        }
    }
}