package com.hb.agenda.utils

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

fun Context.convertDpToPx(`val`: Float): Int {
    val metrics = resources.displayMetrics
    val dp = metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT
    return (dp * `val`).toInt()
}

fun Context.convertDpToPx(`val`: Int): Int {
    val metrics = resources.displayMetrics
    val dp = metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT
    return (dp * `val`).toInt()
}

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(this, provider).get(VM::class.java)

fun Fragment.toast(message: String) {
    requireActivity().toast(message)
}

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}