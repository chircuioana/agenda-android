package com.hb.agenda.utils

import android.text.Editable
import android.widget.EditText

fun EditText.updateTextIfDistinct(newText: String) {
    val text = text?.toString() ?: ""
    if (text != newText) {
        setText(newText)
    }
}