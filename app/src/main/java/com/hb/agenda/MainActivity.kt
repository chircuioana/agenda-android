package com.hb.agenda

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.hb.agenda.ui.addedit.AddOrEditContactFragment
import com.hb.agenda.ui.list.ContactsListFragment
import com.hb.shared.agenda.entities.Contact
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity(), NavigationCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupStatusBar()

        supportFragmentManager
            .beginTransaction()
            .add(R.id.flContainer, ContactsListFragment.newInstance())
            .commit()

//        fab.setOnClickListener {
//            fab.isExpanded = true
//            frBlockBackground.visibility = VISIBLE
//        }
//
//        frBlockBackground.setOnClickListener {
//            fab.isExpanded = false
//            frBlockBackground.visibility = GONE
//        }

        fab.setOnClickListener {
            AddOrEditContactFragment
                .newInstance()
                .show(supportFragmentManager, "AddContactFragment")
        }
    }

    override fun onEditContact(contact: Contact) {
        AddOrEditContactFragment
            .newInstance(contactPhoneNumber = contact.phoneNumber)
            .show(supportFragmentManager, "AddContactFragment")
    }

    private fun setupStatusBar() {
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            val result = resources.getDimensionPixelSize(resourceId)
            root.setPadding(0, result, 0, 0)
        }
    }
}

interface NavigationCallback {
    fun onEditContact(contact: Contact) = Unit
}
