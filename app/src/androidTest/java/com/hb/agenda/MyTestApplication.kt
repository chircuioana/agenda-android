package com.hb.agenda

import com.hb.agenda.MyApplication
import com.hb.agenda.di.DaggerTestAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MyTestApplication: MyApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerTestAppComponent.factory().create(this)
    }
}