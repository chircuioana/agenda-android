package com.hb.agenda.di

import com.hb.agenda.MyApplication
import com.hb.agenda.di.modules.ActivityBindingModule
import com.hb.agenda.di.modules.ViewModelModule
import com.hb.shared.agenda.di.SharedModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        TestAppModule::class,
        TestCoroutinesModule::class,
        SharedModule::class
    ]
)
interface TestAppComponent : AndroidInjector<MyApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: MyApplication): TestAppComponent
    }
}