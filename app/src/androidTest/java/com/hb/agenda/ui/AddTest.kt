package com.hb.agenda.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.hb.agenda.MainActivity
import com.hb.agenda.R
import com.hb.agenda.SyncTaskExecutorRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AddTest {

    @get:Rule
    var activityRule = ActivityTestRule(MainActivity::class.java)

    // Executes tasks in a synchronous [TaskScheduler]
    @get:Rule
    var syncTaskExecutorRule = SyncTaskExecutorRule()

    @Before
    fun goToAddContact() {
        onView(withId(R.id.fab)).perform(click())
    }

    @Test
    fun addContactSuccess() {
        onView(withId(R.id.etFirstName)).perform(typeText("first"))
        onView(withId(R.id.etLastName)).perform(typeText("last"))
        onView(withId(R.id.etPhoneNumber)).perform(typeText("1234567890"), closeSoftKeyboard())

        onView(withId(R.id.btDone)).perform(click())
    }

}