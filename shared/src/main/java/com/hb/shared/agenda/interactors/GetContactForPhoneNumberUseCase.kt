package com.hb.shared.agenda.interactors

import com.hb.shared.agenda.IoDispatcher
import com.hb.shared.agenda.data.ContactsRepository
import com.hb.shared.agenda.entities.Contact
import com.hb.shared.agenda.entities.ERROR_PHONE_NOT_FOUND
import com.hb.shared.agenda.entities.Resource
import com.hb.shared.agenda.utils.FlowUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetContactForPhoneNumberUseCase @Inject constructor(
    private val repository: ContactsRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<GetContactForPhoneNumberUseCase.Params, Contact>(dispatcher) {

    override fun execute(parameters: Params): Flow<Resource<Contact>> {
        return flow {
            try {
                emit(Resource.Loading)
                repository.getContactForPhoneNumber(parameters.phoneNumber)?.let {
                    emit(Resource.Success(it))
                    return@flow
                }
                emit(Resource.Error(code = ERROR_PHONE_NOT_FOUND))
            } catch (e: Exception) {
                emit(Resource.Error(throwable = e))
            }
        }
    }

    data class Params(val phoneNumber: String = "")
}