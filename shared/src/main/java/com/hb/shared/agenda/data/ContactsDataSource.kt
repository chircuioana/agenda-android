package com.hb.shared.agenda.data

import com.hb.shared.agenda.entities.Contact
import kotlinx.coroutines.flow.Flow

interface ContactsDataSource {
    suspend fun add(contact: Contact)
    suspend fun edit(contact: Contact)
    suspend fun delete(contact: Contact)
    suspend fun getForPhoneNumber(phoneNumber: String): Contact?
    suspend fun deleteForPhoneNumber(phoneNumber: String)
    fun getAllAsFlow(query: String = ""): Flow<List<Contact>>
}