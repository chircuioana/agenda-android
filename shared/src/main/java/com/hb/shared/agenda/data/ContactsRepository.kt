package com.hb.shared.agenda.data

import com.hb.shared.agenda.entities.Contact
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ContactsRepository @Inject constructor(private val dataSource: ContactsDataSource) {

    suspend fun addContact(contact: Contact) = dataSource.add(contact)
    suspend fun updateContact(contact: Contact) = dataSource.edit(contact)
    suspend fun removeContact(contact: Contact) = dataSource.delete(contact)


    suspend fun getContactForPhoneNumber(phoneNumber: String): Contact? =
        dataSource.getForPhoneNumber(phoneNumber)
    suspend fun removeContactForPhoneNumber(phoneNumber: String) = dataSource.deleteForPhoneNumber(phoneNumber)

    fun getAllAsFlow(query: String = ""): Flow<List<Contact>> = dataSource.getAllAsFlow(query)


}