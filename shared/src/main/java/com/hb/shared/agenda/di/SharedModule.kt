package com.hb.shared.agenda.di

import com.hb.shared.agenda.data.ContactsDataSource
import com.hb.shared.agenda.data.ContactsRepository
import com.hb.shared.agenda.data.local.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class SharedModule {

    @Singleton
    @Provides
    @Named("contactLocalDataSource")
    fun providesContactLocalDataSource(
        appDatabase: AppDatabase
    ): ContactsDataSource {
        return appDatabase.contactDao() //DummyContactsDataSource()//
    }

    @Singleton
    @Provides
    fun providesContactsRepository(
        @Named("contactLocalDataSource") localDataSource: ContactsDataSource
    ): ContactsRepository {
        return ContactsRepository(localDataSource)
    }
}