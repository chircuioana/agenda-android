package com.hb.shared.agenda.entities

sealed class Resource<out R> {

    data class Success<out T>(val data: T) : Resource<T>()
    data class Error(
        val code: Int? = 0,
        val codes: List<Int>? = null,
        val throwable: Throwable? = null
    ) : Resource<Nothing>()

    object Loading : Resource<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[code:${code}]"
            Loading -> "Loading"
        }
    }
}

val Resource<*>.succeeded
    get() = this is Resource.Success && data != null

fun <T> Resource<T>.successOr(fallback: T): T {
    return (this as? Resource.Success<T>)?.data ?: fallback
}
