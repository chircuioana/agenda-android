package com.hb.shared.agenda.interactors

import com.hb.shared.agenda.IoDispatcher
import com.hb.shared.agenda.data.ContactsRepository
import com.hb.shared.agenda.entities.Contact
import com.hb.shared.agenda.entities.Resource
import com.hb.shared.agenda.utils.FlowUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetAllContactsUseCase @Inject constructor(
    private val repository: ContactsRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) :
    FlowUseCase<GetAllContactsUseCase.Params, List<Contact>>(dispatcher) {
    override fun execute(parameters: Params): Flow<Resource<List<Contact>>> {
        return flow {
            try {
                emit(Resource.Loading)
                repository.getAllAsFlow(parameters.query).collect {
                    emit(Resource.Success(it))
                }
            } catch (e: Exception) {
                emit(Resource.Error(throwable = e))
            }
        }
    }

    data class Params(val query: String = "")
}