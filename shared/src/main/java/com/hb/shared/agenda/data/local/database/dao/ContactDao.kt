package com.hb.shared.agenda.data.local.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.hb.shared.agenda.data.ContactsDataSource
import com.hb.shared.agenda.entities.CONTACTS_TABLE
import com.hb.shared.agenda.entities.Contact
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

@Dao
abstract class ContactDao : ContactsDataSource {

    @Insert(onConflict = REPLACE)
    abstract suspend fun insertAsync(contact: Contact)

    @Delete
    abstract suspend fun deleteAsync(contact: Contact)

    @Query("SELECT * FROM $CONTACTS_TABLE ORDER BY firstName ASC")
    abstract fun getAllAsync(): Flow<List<Contact>>

    @Query("SELECT * FROM $CONTACTS_TABLE WHERE phoneNumber = :phoneNumber LIMIT 1")
    abstract fun getContactForPhoneNumber(phoneNumber: String): Contact?

    @Query("DELETE FROM $CONTACTS_TABLE WHERE phoneNumber = :phoneNumber")
    abstract fun deleteContactForPhoneNumber(phoneNumber: String): Int

    override suspend fun add(contact: Contact) {
        insertAsync(contact)
    }

    override suspend fun edit(contact: Contact) {
        insertAsync(contact)
    }

    override suspend fun delete(contact: Contact) {
        deleteAsync(contact)
    }

    override suspend fun getForPhoneNumber(phoneNumber: String): Contact? =
        withContext(Dispatchers.IO) {
            return@withContext getContactForPhoneNumber(phoneNumber)
        }

    override suspend fun deleteForPhoneNumber(phoneNumber: String) {
        withContext(Dispatchers.IO) {
            return@withContext deleteContactForPhoneNumber(phoneNumber)
        }
    }

    override fun getAllAsFlow(query: String): Flow<List<Contact>> {
        return getAllAsync()
    }

}