package com.hb.shared.agenda.interactors

import com.hb.shared.agenda.IoDispatcher
import com.hb.shared.agenda.data.ContactsRepository
import com.hb.shared.agenda.entities.Contact
import com.hb.shared.agenda.entities.Resource
import com.hb.shared.agenda.utils.FlowUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RemoveContactUseCase @Inject constructor(
    private val repository: ContactsRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<RemoveContactUseCase.Params, Unit>(dispatcher) {
    override fun execute(parameters: Params): Flow<Resource<Unit>> {
        return flow {
            try {
                emit(Resource.Loading)
                repository.removeContactForPhoneNumber(parameters.phoneNumber)
                emit(Resource.Success(Unit))

            } catch (e: Exception) {
                emit(Resource.Error(throwable = e))
            }
        }
    }

    data class Params(val phoneNumber: String)
}