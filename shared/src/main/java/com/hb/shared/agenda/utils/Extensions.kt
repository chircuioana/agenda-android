package com.hb.shared.agenda.utils

import androidx.core.util.PatternsCompat
import java.util.regex.Pattern

fun String.isPhoneNumberValid(): Boolean {
    val pattern = Pattern.compile(
        "(\\+[0-9]+[\\- \\.]*)?"
                + "(\\([0-9]+\\)[\\- \\.]*)?"
                + "([0-9][0-9\\- \\.]+[0-9])"
    )
    return pattern.matcher(this).matches()
}

fun String.isEmailValid(): Boolean {
    return PatternsCompat.EMAIL_ADDRESS.matcher(this).matches()
}

