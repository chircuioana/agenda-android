package com.hb.shared.agenda.entities

import androidx.annotation.StringRes

const val ERROR_UNKNOWN = 4000
const val ERROR_FIRST_NAME_IS_EMPTY = 4001
const val ERROR_PHONE_IS_EMPTY = 4002
const val ERROR_PHONE_FORMAT_INVALID = 4003
const val ERROR_PHONE_EXISTS_ALREADY = 4004
const val ERROR_LAST_NAME_IS_EMPTY = 4005
const val ERROR_EMAIL_INVALID = 4006
const val ERROR_PHONE_NOT_FOUND = 4007

sealed class Error(
    val code: Int? = 0,
    @StringRes
    val messageRes: Int? = 0
) {
}

