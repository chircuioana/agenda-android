package com.hb.shared.agenda.interactors

import com.hb.shared.agenda.IoDispatcher
import com.hb.shared.agenda.data.ContactsRepository
import com.hb.shared.agenda.entities.*
import com.hb.shared.agenda.utils.FlowUseCase
import com.hb.shared.agenda.utils.isEmailValid
import com.hb.shared.agenda.utils.isPhoneNumberValid
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AddContactUseCase @Inject constructor(
    private val contactsRepository: ContactsRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<AddContactUseCase.Params, Unit>(dispatcher) {

    data class Params(
        val firstName: String?,
        val lastName: String?,
        val phoneNumber: String?,
        val email: String?
    )

    override fun execute(parameters: Params): Flow<Resource<Unit>> {
        return flow {
            try {
                emit(Resource.Loading)

                val errors = collectErrors(
                    firstName = parameters.firstName,
                    lastName = parameters.lastName,
                    phoneNumber = parameters.phoneNumber,
                    email = parameters.email
                )

                if (errors.isNotEmpty()) {
                    emit(Resource.Error(codes = errors))
                    return@flow
                }

                contactsRepository.addContact(
                    Contact(
                        phoneNumber = parameters.phoneNumber!!,
                        firstName = parameters.firstName!!,
                        lastName = parameters.lastName!!,
                        email = parameters.email
                    )
                )
                emit(Resource.Success(Unit))
            } catch (e: Exception) {
                emit(Resource.Error(throwable = e))
            }
        }
    }

    private suspend fun collectErrors(
        firstName: String?,
        lastName: String?,
        phoneNumber: String?,
        email: String?
    ): List<Int> {
        val errors = arrayListOf<Int>()

        (firstName ?: "").run {
            if (trim().isEmpty()) {
                errors.add(ERROR_FIRST_NAME_IS_EMPTY)
            }
        }

        (lastName ?: "").run {
            if (trim().isEmpty()) {
                errors.add(ERROR_LAST_NAME_IS_EMPTY)
            }
        }

        (email ?: "").run {
            if (trim().isEmpty()) {
                return@run
            }
            if (!trim().isEmailValid()) {
                errors.add(ERROR_EMAIL_INVALID)
            }
        }

        (phoneNumber ?: "").run {
            if (trim().isEmpty()) {
                errors.add(ERROR_PHONE_IS_EMPTY)
            }
            if (!trim().isPhoneNumberValid()) {
                errors.add(ERROR_PHONE_FORMAT_INVALID)
            }
            if (contactsRepository.getContactForPhoneNumber(this) != null) {
                errors.add(ERROR_PHONE_EXISTS_ALREADY)
            }
        }

        return errors
    }
}