package com.hb.shared.agenda.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

const val CONTACTS_TABLE = "CONTACTS_TABLE"

@Entity(tableName = CONTACTS_TABLE)
data class Contact(
    @PrimaryKey
    val phoneNumber: String,
    val firstName: String,
    val lastName: String,
    val email: String? = ""
) {

    val displayName: String
        get() {
            return "$firstName $lastName".trim()
        }
}