package com.hb.shared.agenda.data

import com.hb.shared.agenda.entities.Contact
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeContactsDataSource : ContactsDataSource {

    val dummy = arrayListOf(
        Contact("083328957923", "Testi", "Mctest", null),
        Contact("083328957921", "Testi", "Mctest", null),
        Contact("083328957922", "Testi", "Mctest", null),
        Contact("083328957924", "Testi", "Mctest", null),
        Contact("083328957925", "Testi", "Mctest", null),
        Contact("083328957926", "Testi", "Mctest", null),
        Contact("083328957927", "Testi", "Mctest", null),
        Contact("083328957928", "Testi", "Mctest", null),
        Contact("083328957929", "Testi", "Mctest", null),
        Contact("083328957920", "Testi", "Mctest", null),
        Contact("012345", "Testi", "Mctest", null)
    )

    override suspend fun add(contact: Contact) {

    }

    override suspend fun edit(contact: Contact) {

    }

    override suspend fun delete(contact: Contact) {

    }

    override suspend fun getForPhoneNumber(phoneNumber: String): Contact? {
        return dummy.firstOrNull { contact -> contact.phoneNumber == phoneNumber }
    }

    override suspend fun deleteForPhoneNumber(phoneNumber: String) {
        dummy.removeIf { it.phoneNumber == phoneNumber }
    }

    override fun getAllAsFlow(query: String): Flow<List<Contact>> {
        return flow { emit(getAll(query)) }
    }

    private fun getAll(query: String): List<Contact> {
        return dummy
    }
}