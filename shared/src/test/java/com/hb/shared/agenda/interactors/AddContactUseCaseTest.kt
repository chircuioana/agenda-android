package com.hb.shared.agenda.interactors

import com.hb.shared.agenda.MainCoroutineRule
import com.hb.shared.agenda.data.ContactsRepository
import com.hb.shared.agenda.data.FakeContactsDataSource
import com.hb.shared.agenda.entities.*
import com.hb.shared.agenda.runBlockingTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.instanceOf
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.Before

@ExperimentalCoroutinesApi
class AddContactUseCaseTest {
    @get:Rule
    var coroutineRule = MainCoroutineRule()

    lateinit var addContactUseCase: AddContactUseCase
    private lateinit var contactsRepository: ContactsRepository

    @Before
    fun build() {
        contactsRepository = ContactsRepository(FakeContactsDataSource())
        addContactUseCase = AddContactUseCase(contactsRepository, coroutineRule.testDispatcher)
    }

    @Test
    fun `First name is null`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                null,
                "test",
                "1234567890",
                null
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }
            assert(it is Resource.Error && it.codes?.contains(ERROR_FIRST_NAME_IS_EMPTY) == true)
        }
    }

    @Test
    fun `Last name is null`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                "test",
                null,
                "1234567890",
                null
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }
            assert(it is Resource.Error && it.codes?.contains(ERROR_LAST_NAME_IS_EMPTY) == true)
        }
    }

    @Test
    fun `Phone number is empty`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                "test",
                "test",
                "",
                null
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }

            assert(it is Resource.Error && it.codes?.contains(ERROR_PHONE_IS_EMPTY) == true)
        }
    }

    @Test
    fun `Phone number is invalid`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                "test",
                "test",
                "test",
                null
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }

            assert(it is Resource.Error && it.codes?.contains(ERROR_PHONE_FORMAT_INVALID) == true)
        }
    }

    @Test
    fun `Email is invalid`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                "test",
                "test",
                "1234567890",
                " test     "
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }

            assert(it is Resource.Error && it.codes?.contains(ERROR_EMAIL_INVALID) == true)
        }
    }

    @Test
    fun `Phone already exists`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                "test",
                "test",
                "012345",
                null
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }

            assert(it is Resource.Error && it.codes?.contains(ERROR_PHONE_EXISTS_ALREADY) == true)
        }
    }


    @Test
    fun `Added contact successfully`() = coroutineRule.runBlockingTest {
        addContactUseCase(
            AddContactUseCase.Params(
                "test",
                "test",
                "12345678910",
                null
            )
        ).collect {
            if (it is Resource.Loading) {
                return@collect
            }
            assertThat(it, `is`(instanceOf(Resource.Success::class.java)))
        }
    }
}

